from django.contrib import admin
from league.models import *


# Register your models here.


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = ["name", "team_logo"]
    search_fields = ['name']
    pass


@admin.register(Coach)
class CoachAdmin(admin.ModelAdmin):
    pass


@admin.register(Game)
class GameAdmin(admin.ModelAdmin):
    pass


@admin.register(Player)
class PlayerAdmin(admin.ModelAdmin):
    list_display = ["name", "player_photo"]
    list_filter = ('birth_date', 'team')
    search_fields = ['name', 'alias', 'rut']


@admin.register(Roster)
class RosterAdmin(admin.ModelAdmin):
    pass
