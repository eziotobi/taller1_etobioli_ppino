from django.shortcuts import render
from league.models import Game, Player, Team, Coach
from django.db.models import Count


def home(request):
    template_name = 'home.html'
    data = {}

    data['games'] = Game.objects.all().reverse()[0:5]
    data['last_players'] = Player.objects.all().reverse()[0:5]

    teams_total = Player.objects.all().values('team').annotate(total=Count('name')).order_by('total')

    data['team_players'] = {}
    for team in teams_total:
        team_object = Team.objects.get(pk=team['team'])
        data['team_players']['' + str(team['team'])] = {
            'name': team_object.name,
            'total_players': team['total']
        }

    for player in data['team_players']:
        print(player)

    return render(request, template_name, data)


def players(request):
    template_name = 'players.html'
    data = {}

    teams = Team.objects.all()
    teams_data = {}

    for team in teams:
        teams_data[team.name] = Player.objects.all().filter(team=team.id)

    data['teams'] = teams_data

    print(data['teams'])

    return render(request, template_name, data)


def coach(request):
    template_name = 'coach.html'
    data = {}

    coaches = Coach.objects.all()

    coach_team = {}

    for coach in coaches:
        coach_team[coach.name] = Team.objects.get(coach_id=coach.id).name

    data['coaches'] = coach_team

    return render(request, template_name, data)


def games(request):
    template_name = 'games.html'
    data = {}

    data['last_games'] = Game.objects.all().reverse()


    return render(request, template_name, data)
