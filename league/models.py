from django.db import models
from django.utils.safestring import mark_safe


class Team(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    photo = models.ImageField(upload_to='pics', max_length=500, blank=True, null=True)
    coach = models.ForeignKey('Coach', on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    def team_logo(self):
        return mark_safe('<img src="/media/%s" style="width:100px"/>' % self.photo)


class Player(models.Model):
    name = models.CharField(max_length=50)
    alias = models.CharField(max_length=20)
    birth_date = models.DateField()
    age = models.IntegerField()
    rut = models.CharField(max_length=11)
    email = models.EmailField()
    height = models.IntegerField()
    weight = models.DecimalField(decimal_places=2, max_digits=10)
    photo = models.ImageField(upload_to='pics', max_length=500)
    game_position = [
        (0, "Base"),
        (1, "Escolta"),
        (2, "Alero"),
        (3, "Ala-pivot"),
        (4, "Pivot")

    ]
    team = models.ForeignKey('Team', on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    def player_photo(self):
        return mark_safe('<img src="/media/%s" style="width:100px"/>' % self.photo)


class Coach(models.Model):
    name = models.CharField(max_length=50)
    alias = models.CharField(max_length=20)
    age = models.IntegerField()
    rut = models.CharField(max_length=11)
    email = models.EmailField()

    def __str__(self):
        return self.name


class Game(models.Model):
    name = models.CharField(max_length=200)
    team1 = models.ForeignKey('Roster', related_name='team1', on_delete=models.CASCADE, null=True, blank=True)
    team2 = models.ForeignKey('Roster', related_name='team2', on_delete=models.CASCADE, null=True, blank=True)
    date = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return '{name} ({team1} vs {team2})'.format(
            name=self.name,
            team1=self.team1,
            team2=self.team2
        )


class Roster(models.Model):
    game_date = models.DateField()
    players = models.ManyToManyField('Player')

    def __str__(self):
        return "Nomina - " + str(self.game_date)
